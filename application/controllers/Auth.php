<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct()
  {
    parent::__construct();
    $this->load->model('user_model', 'usr');
  }

  public function index()
  {
    $this->form_validation->set_rules('username', 'Username', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $data['title'] = 'aplikasi kurniawati';
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/auth_header', $data);
      $this->load->view('auth/login', $data);
      $this->load->view('templates/auth_footer');
    } else {
      $this->_login();
    }
  }

  private function _login()
  {
    $input = $this->input->post(null, true);
    $username = $input['username'];
    $password = $input['password'];
    $user = $this->usr->getLogin($username);

    if ($user) {

      if ($user['is_active'] == 1) {

        if (password_verify($password, $user['user_password'])) {
          $data = [
            'user_email' => $user['user_email'],
            'user_role_id' => $user['user_role_id']
          ];
          $this->session->set_userdata($data);
          redirect('master');
        } else {
          echo 'password salah';
        }
      } else {
        echo 'data user tersuspend';
      }
    } else {
      echo 'data tidak ada';
    }
  }

  public function register()
  {
    $this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.user_name]', [
      'is_unique' => 'Username is exist'
    ]);
    $this->form_validation->set_rules('email', 'Email', 'required|is_unique[user.user_email]', [
      'is_unique' => 'Your email is exist'
    ]);
    $this->form_validation->set_rules('password1', 'Password1', 'required|min_length[8]|trim|matches[password2]', [
      'matches' => 'Password not Macth',
      'min_length' => 'Password must be at least 8 characters in length.'
    ]);
    $this->form_validation->set_rules('password2', 'Password2', 'required|min_length[8]|trim|matches[password1]', [
      'matches' => 'Password not Macth',
      'min_length' => 'Password must be at least 8 characters in length.'
    ]);
    if ($this->form_validation->run() == false) {
      $data['title'] = 'aplikasi kurniawati';
      $this->load->view('templates/auth_header', $data);
      $this->load->view('auth/register', $data);
      $this->load->view('templates/auth_footer', $data);
    } else {
      $input = $this->input->post(null, true);
      $datauser = [
        'user_name' => $input['username'],
        'user_email' => $input['email'],
        'user_password' => password_hash($input['password1'], PASSWORD_DEFAULT),
        'is_active' => 1,
        'user_role_id' => 1,
        'date_created' => date('Y/m/d')
      ];

      $this->usr->getAuthregister($datauser);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Registrasi Berhasil!</div>');
      redirect('auth');
    }
  }

  public function logout()
  {
    $this->session->unset_userdata('user_email');
    $this->session->unset_userdata('user_role_id');
    redirect('auth');
  }
}
