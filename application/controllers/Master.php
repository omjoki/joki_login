<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model', 'usr');
		$this->load->model('kantorpos_model', 'kps');
		$this->load->model('antaran_model', 'atr');
	}

	public function index()
	{
		redirect('master/penerima');
	}

	public function penerima()
	{
		$data['user'] = $this->db->get_where('user', ['user_email' => $this->session->userdata('user_email')])->row_array();
		$data['namamenu'] = 'Master';
		$data['namasubmenu'] = 'Master Penerima';
		$data['datapenerima'] = $this->atr->getPenerima();
		$this->load->view('templates/dashboard_header', $data);
		$this->load->view('master/penerima', $data);
		$this->load->view('templates/dashboard_footer');
	}

	public function tambahpenerima()
	{
		$data['user'] = $this->db->get_where('user', ['user_email' => $this->session->userdata('user_email')])->row_array();
		$data['namamenu'] = 'Master';
		$data['namasubmenu'] = 'Master Penerima';
		$data['provinsi'] = $this->usr->getProvinsi();
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('telp', 'Telp', 'required');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
		$this->form_validation->set_rules('kotakabupaten', 'Kotakabupaten', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('kelurahan', 'Kelurahan', 'required');
		if ($this->form_validation->run() == false) {
			$this->load->view('templates/dashboard_header', $data);
			$this->load->view('master/tambahpenerima', $data);
			$this->load->view('templates/dashboard_footer');
		} else {
			$input = $this->input->post(null, true);
			$datapenerima = [
				'tb_penerima_nama' => $input['nama'],
				'tb_penerima_alamat' => $input['alamat'],
				'tb_penerima_telp' => $input['telp'],
				'tb_penerima_provinsi' => $input['provinsi'],
				'tb_penerima_kecamatan' => $input['kecamatan'],
				'tb_penerima_kelurahan' => $input['kelurahan'],
				'tb_penerima_kabupaten_kota' => $input['kotakabupaten'],
			];
			$this->atr->getInsertpenerima($datapenerima);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Penerima Berhasil di Tambah!</div>');
			redirect('master/penerima');
		}
	}

	public function editpenerima()
	{
		$data['user'] = $this->db->get_where('user', ['user_email' => $this->session->userdata('user_email')])->row_array();
		$data['namamenu'] = 'Master';
		$data['namasubmenu'] = 'Master Penerima';
		$data['provinsi'] = $this->usr->getProvinsi();
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('telp', 'Telp', 'required');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
		$this->form_validation->set_rules('kotakabupaten', 'Kotakabupaten', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('kelurahan', 'Kelurahan', 'required');
		if ($this->form_validation->run() == false) {
			$this->load->view('templates/dashboard_header', $data);
			$this->load->view('master/tambahpenerima', $data);
			$this->load->view('templates/dashboard_footer');
		} else {
			$input = $this->input->post(null, true);
			$datapenerima = [
				'tb_penerima_nama' => $input['nama'],
				'tb_penerima_alamat' => $input['alamat'],
				'tb_penerima_telp' => $input['telp'],
				'tb_penerima_provinsi' => $input['provinsi'],
				'tb_penerima_kecamatan' => $input['kecamatan'],
				'tb_penerima_kelurahan' => $input['kelurahan'],
				'tb_penerima_kabupaten_kota' => $input['kotakabupaten'],
			];
			$this->atr->getInsertpenerima($datapenerima);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Penerima Berhasil di Tambah!</div>');
			redirect('master/penerima');
		}
	}

	public function getkotakabupaten()
	{
		$id = $this->input->post('id');

		$data = [
			'id_provinsi' => $id
		];

		$result = $this->db->get_where('tb_kabupaten_kota', $data)->result_array();
		$hasil = '<option value=""> -- Pilih Kabupaten Kota -- </option>';
		foreach ($result as $r) {
			$hasil .= '<option value="' . $r['id_kabupaten_kota'] . '">' . $r['name'] . '</option>';
		};
		echo json_encode($hasil);
	}

	public function getkecamatan()
	{
		$id = $this->input->post('id');

		$data = [
			'id_kabupaten_kota' => $id
		];

		$result = $this->db->get_where('tb_kecamatan', $data)->result_array();
		$hasil = '<option value=""> -- Pilih Kecamatan -- </option>';
		foreach ($result as $r) {
			$hasil .= '<option value="' . $r['id_kecamatan'] . '">' . $r['name'] . '</option>';
		};
		echo json_encode($hasil);
	}

	public function getkelurahan()
	{
		$id = $this->input->post('id');

		$data = [
			'id_kecamatan' => $id
		];

		$result = $this->db->get_where('tb_kelurahan', $data)->result_array();
		$hasil = '<option value=""> -- Pilih Kelurahan -- </option>';
		foreach ($result as $r) {
			$hasil .= '<option value="' . $r['id_kelurahan'] . '">' . $r['name'] . '</option>';
		};
		echo json_encode($hasil);
	}

	public function hapuspenerima($tb_penerima_id)
	{
		$this->db->where('tb_penerima_id', $tb_penerima_id);
		$this->db->delete('tb_penerima');
		redirect('master/penerima');
	}
}
