<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('latihan/index');
	}

	public function tambahkelompok()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		if ($this->form_validation->run() == false) {
			$this->load->view('latihan/tambahkelompok');
		} else {
			$datakelompok = [
				'tb_latihan_kelompok_nama' => $this->input->post('nama')
			];
			$this->db->insert('tb_latihan_kelompok', $datakelompok);
			redirect('welcome');
		}
	}

	public function hapuskelompok($kelompokid)
	{
		$this->db->where('tb_latihan_kelompok_id', $kelompokid);
		$this->db->delete('tb_latihan_kelompok');
		redirect('welcome');
	}
}
