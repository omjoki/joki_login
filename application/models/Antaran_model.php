<?php

class Antaran_model extends CI_Model
{
  public function getAntaran($antaranid = false)
  {
    if (!$antaranid) {
      return $this->db->get('tb_antaran')->result_array();
    } else {
      $this->db->where('tb_antaran_id', $antaranid);
      return $this->db->get('tb_antaran')->row_array();
    }
  }

  public function getPenerima($penerimaid = false)
  {
    if (!$penerimaid) {
      return $this->db->get('tb_penerima')->result_array();
    } else {
      $this->db->where('tb_penerima_id', $penerimaid);
      return $this->db->get('tb_penerima')->row_array();
    }
  }

  public function getInsertpenerima($datapenerima)
  {
    $this->db->insert('tb_penerima', $datapenerima);
  }

  public function getAntaranfile($antaranfileid = false)
  {
    if (!$antaranfileid) {
      return $this->db->get('tb_antaran_file')->result_array();
    } else {
      $this->db->where('tb_antaran_file_id', $antaranfileid);
      return $this->db->get('tb_antaran_file')->row_array();
    }
  }

  public function getAntarandetail($da)
  {
    $this->db->where('tb_antaran_id', $da);
    return $this->db->get('tb_antaran_detail')->result_array();
  }

  public function getStatus($da)
  {
    $this->db->where('tb_antaran_status_id', $da);
    return $this->db->get('tb_antaran_status')->row_array();
  }
}
