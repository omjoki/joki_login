<?php

class Kantorpos_model extends CI_Model
{
  public function getMenu()
  {
    $role_id = $this->session->userdata('user_role_id');
    $queryMenu = "SELECT user_menu.user_menu_id, user_menu_title, user_menu_icon
        FROM user_menu JOIN user_access_menu
        ON user_menu.user_menu_id = user_access_menu.user_menu_id
        WHERE user_access_menu.user_role_id = $role_id
        ORDER BY user_access_menu.user_menu_id ASC
        ";
    return $this->db->query($queryMenu)->result_array();
  }

  public function getSubmenu($m)
  {
    return $this->db->get_where('user_sub_menu', ['user_menu_id' => $m])->result_array();
  }
}
