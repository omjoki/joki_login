<?php

class User_model extends CI_Model
{
	public function getAuthregister($datauser)
	{
		$this->db->insert('user', $datauser);
	}

	public function getLogin($username)
	{
		$this->db->where('user_name', $username);
		return $this->db->get('user')->row_array();
	}

	public function getProvinsi()
	{
		return $this->db->get('tb_provinsi')->result_array();
	}

	public function getUserdataprovinsi($provinsiid)
	{
		return $this->db->get_where('tb_provinsi', ['id_provinsi' => $provinsiid])->row_array();
	}

	public function getUserdatakabupatenkota($kotakabid)
	{
		return $this->db->get_where('tb_kabupaten_kota', ['id_kabupaten_kota' => $kotakabid])->row_array();
	}

	public function getUserdatakecamatan($kecamatanid)
	{
		return $this->db->get_where('tb_kecamatan', ['id_kecamatan' => $kecamatanid])->row_array();
	}

	public function getUserdatakelurahan($kelurahanid)
	{
		return $this->db->get_where('tb_kelurahan', ['id_kelurahan' => $kelurahanid])->row_array();
	}

	public function getUser($userid = false)
	{
		if (!$userid) {
			return $this->db->get('user')->result_array();
		} else {
			$this->db->where('user_id', $userid);
			return $this->db->get('user')->row_array();
		}
	}

	public function getUserrole($userroleid = false)
	{
		if (!$userroleid) {
			return $this->db->get('user_role')->result_array();
		} else {
			$this->db->where('user_role_id', $userroleid);
			return $this->db->get('user_role')->row_array();
		}
	}
}
