<!-- /.login-logo -->
<div class="card">
  <div class="card-body login-card-body">
    <p class="login-box-msg">Register to Admin</p>

    <form action="<?= base_url('auth/register'); ?>" method="post">
      <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
      <div class="input-group mb-3">
        <input type="text" class="form-control" name="username" placeholder="Username" value="<?= set_value('username'); ?>">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-user"></span>
          </div>
        </div>
      </div>
      <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
      <div class="input-group mb-3">
        <input type="email" class="form-control" name="email" placeholder="Email" value="<?= set_value('email'); ?>">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-envelope"></span>
          </div>
        </div>
      </div>
      <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
      <div class="input-group mb-3">
        <input type="password" class="form-control" name="password1" placeholder="Password">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>
      </div>
      <?= form_error('password2', '<small class="text-danger pl-3">', '</small>'); ?>
      <div class="input-group mb-3">
        <input type="password" class="form-control" name="password2" placeholder="Repeat Password">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-8">
          <div class="icheck-primary">
            <input type="checkbox" id="remember">
            <label for="remember">
              Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-4">
          <button type="submit" class="btn btn-primary btn-block">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center mb-3">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-primary">
        <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
      </a>
      <a href="#" class="btn btn-block btn-danger">
        <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
      </a>
    </div>
    <!-- /.social-auth-links -->

    <p class="mb-1">
      <a href="forgot-password.html">I forgot my password</a>
    </p>
    <p class="mb-0">
      <a href="register.html" class="text-center">Register a new membership</a>
    </p>
  </div>
  <!-- /.login-card-body -->
</div>