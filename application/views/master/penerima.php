<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <!-- Left col -->

      <section class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-chart-pie mr-1"></i>
              Antaran
            </h3>
            <div class="card-tools">
              <ul class="nav nav-pills ml-auto">
                <li class="nav-item">
                  <a class="btn btn-primary" href="<?= base_url('master/tambahpenerima'); ?>">Tambah Data Penerima</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="card-body">
            <div class="tab-content p-0">
              <table id="example2" class="table table-bordered table-hover" style="width: 100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Penerima</th>
                    <th>Alamat Penerima</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  <?php foreach ($datapenerima as $dp) : ?>
                    <?php
                    $user_provinsi = $this->usr->getUserdataprovinsi($dp['tb_penerima_provinsi']);
                    $user_kabupaten = $this->usr->getUserdatakabupatenkota($dp['tb_penerima_kabupaten_kota']);
                    $user_kecamatan = $this->usr->getUserdatakecamatan($dp['tb_penerima_kecamatan']);
                    $user_kelurahan = $this->usr->getUserdatakelurahan($dp['tb_penerima_kelurahan']);
                    ?>
                    <tr>
                      <td><?= $i; ?></td>
                      <td>
                        <?= $dp['tb_penerima_nama']; ?>
                      </td>
                      <td>
                        <?= $dp['tb_penerima_alamat'] . ', ' . $user_kelurahan['name'] . ', ' . $user_kecamatan['name'] . ', ' . $user_kabupaten['name'] . ', ' . $user_provinsi['name']; ?>
                      </td>
                      <td>
                        <a href="" class="btn btn-success">Edit Penerima</a>
                        <a href="<?= base_url('master/hapuspenerima/') . $dp['tb_penerima_id']; ?>" class="btn btn-danger">Hapus Penerima</a>
                      </td>
                    </tr>
                    <?php $i++; ?>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
      </section>
      <!-- /.Left col -->
      <!-- right col (We are only adding the ID to make the widgets sortable)-->
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->