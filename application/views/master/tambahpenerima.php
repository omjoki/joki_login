<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->

      <section class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-chart-pie mr-1"></i>
              Penerima
            </h3>
          </div>
          <div class="card-body">
            <form action="<?= base_url('master/tambahpenerima'); ?>" method="post">
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Nama Penerima</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Penerima" value="<?= set_value('nama'); ?>">
                  <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Telp</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="telp" name="telp" placeholder="Telp" value="<?= set_value('telp'); ?>">
                  <?= form_error('telp', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" value="<?= set_value('alamat'); ?>">
                  <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Provinsi</label>
                <div class="col-sm-10">
                  <select class="form-control" name="provinsi" id="provinsi">
                    <option value="">-- Pilih Provinsi --</option>
                    <?php foreach ($provinsi as $p) : ?>
                      <option value="<?= $p['id_provinsi']; ?>"><?= $p['name'] ?></option>
                    <?php endforeach; ?>
                  </select>
                  <?= form_error('provinsi', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Kota Kabupaten</label>
                <div class="col-sm-10">
                  <select class="form-control" id="kotakabupaten" name="kotakabupaten">
                    <option value="">-- Pilih Kabupaten Kota --</option>
                  </select>
                  <?= form_error('kotakabupaten', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Kecamatan</label>
                <div class="col-sm-10">
                  <select class="form-control" id="kecamatan" name="kecamatan">
                    <option value="">-- Pilih Kecamatan --</option>
                  </select>
                  <?= form_error('kecamatan', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Kelurahan</label>
                <div class="col-sm-10">
                  <select class="form-control" id="kelurahan" name="kelurahan">
                    <option value="">-- Pilih Kelurahan --</option>
                  </select>
                  <?= form_error('kelurahan', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
              </div>
              <button class="btn btn-success" type="submit">Tambah Penerima</button>
            </form>
          </div>
      </section>
      <!-- /.Left col -->
      <!-- right col (We are only adding the ID to make the widgets sortable)-->
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>